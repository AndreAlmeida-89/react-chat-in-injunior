import React from 'react';
import './App.css';
import Header from './components/Header';
import Messages from './components/Messages';
import InputMessage from './components/InputMessage'



function App() {
  return (
    <div className="App">
      <Header />
      <InputMessage />
      <Messages />
    </div>
  );
}

export default App;
