import React, {useState, useEffect} from 'react';
import axios from 'axios';

const api = axios.create({
    baseURL: 'https://treinamentoajax.herokuapp.com/'
})

function DataFetching() {
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await api.get('messages');
            const responseArray = response.data;
            setMessages(responseArray);
        }
        fetchData();
    }, [])

    return (
        <div className="chat">  
            {messages.map(item => (
                <div key={item.id} className="single-message">
                    <h2>{item.name}</h2>
                    <p>{item.message}</p>
                </div>
            ))}     
         </div>
    )
}

export default DataFetching
