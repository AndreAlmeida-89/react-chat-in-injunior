import React, {useState, useEffect} from 'react';
import axios from 'axios';



function InputMessage() {

    const [name, setName] = useState('');
    const [message, setMessage] = useState('');
    let fetchBody = {
        "message":{
            "name": name,
            "message": message
        }
    }
    const submitForm = (e) =>{
        e.preventDefault();
        axios.post('https://treinamentoajax.herokuapp.com/messages', fetchBody)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        })
    };    
    
    return (
        <div className="form-inline">
            <form onSubmit={submitForm}>
                <h1>Olá!📨</h1>
                <label for="name">Digite seu nome:</label>
                <input type="text" id="name" required 
                    onChange={(e)=> setName(e.target.value)} 
                />
                <label for="textarea">Digite sua mensagem:</label>
                <textarea id="textarea" placeholder="Escreva algo legal..." required 
                    onChange={(e)=> setMessage(e.target.value)}>
                </textarea>
                <input type="submit" value="Enviar" className="btn btn-primary"></input>
            </form>
        </div>
    )
}

export default InputMessage
