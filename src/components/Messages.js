import React from 'react';
import DataFetching from './DataFetching';

export default function Messages(){
    return(
        <div className="messages">
            <h1>Mensagens postadas:</h1>
            <DataFetching />
        </div>
    );
}