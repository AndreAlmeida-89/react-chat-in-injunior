import React from 'react';

export default function Header(){
    return(
        <header>
            <h1 className="display-1">Chatz{'{'}IN{'}'}</h1>
            <button className="btn btn-primary">Home</button>
        </header>
    );
}